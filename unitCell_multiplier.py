print "--------------------------------"
print "FCC UNIT CELL MULTIPLIER v1.2"
print "--------------------------------\n"

outfile=raw_input("Enter Output File Name:")
fout=open(outfile+".xyz","w")

n=14
a=input("Edge Length of unit cell:")
px=input("Increment in X:")
py=input("Increment in Y:")
pz=input("Increment in Z:")
fcc=['#Generated FCC Unit cell\n', 'C 0.0 0.0 0.0\n', 'C '+str(a)+' 0.0 0.0\n', 'C 0.0 '+str(a)+' 0.0\n', 'C 0.0 0.0 '+str(a)+'\n', 'C '+str(a)+' '+str(a)+' 0.0\n', 'C '+str(a)+' 0.0 '+str(a)+'\n', 'C 0.0 '+str(a)+' '+str(a)+'\n', 'C '+str(a)+' '+str(a)+' '+str(a)+'\n', 'C '+str(a/2.0)+' '+str(a/2.0)+' 0.0\n', 'C '+str(a/2.0)+' '+str(a/2.0)+' '+str(a)+'\n', 'C 0.0 '+str(a/2.0)+' '+str(a/2.0)+'\n', 'C '+str(a)+' '+str(a/2.0)+' '+str(a/2.0)+'\n', 'C '+str(a/2.0)+' 0.0 '+str(a/2.0)+'\n', 'C '+str(a/2.0)+' '+str(a)+' '+str(a/2.0)+'\n']
n=n+9*px+py*(9+6*(px+1))+pz*(9+6*(py+1)+6*(px+1)*(py+1))
fout.write(str(n)+"\n")	

for i in xrange(0,len(fcc)):
	fout.write(fcc[i])

if(px!=0):
	for i in xrange(0,px):
		fout.write("C "+str((i+2)*a)+" "+str(a)+" 0.0\n")
		fout.write("C "+str((i+2)*a)+" 0.0 0.0\n")
		fout.write("C "+str((i+2)*a)+" 0.0 "+str(a)+"\n")
		fout.write("C "+str((i+2)*a)+" "+str(a)+" "+str(a)+"\n")
		fout.write("C "+str((i+2)*a)+" "+str(a/2.0)+" "+str(a/2.0)+"\n")
		fout.write("C "+str(((i+2)*a)-a/2.0)+" "+str(a)+" "+str(a/2.0)+"\n")
		fout.write("C "+str(((i+2)*a)-a/2.0)+" "+"0.0"+" "+str(a/2.0)+"\n")
		fout.write("C "+str(((i+2)*a)-a/2.0)+" "+str(a/2.0)+" "+str(a)+"\n")
		fout.write("C "+str(((i+2)*a)-a/2.0)+" "+str(a/2.0)+" "+"0.0"+"\n")



if(py!=0):
	for i in xrange(0,py):
		fout.write("C "+str(a)+" "+str((i+2)*a)+" 0.0\n")
		fout.write("C 0.0 "+str((i+2)*a)+" 0.0\n")
		fout.write("C 0.0 "+str((i+2)*a)+" "+str(a)+"\n")
		fout.write("C "+str(a)+" "+str((i+2)*a)+" "+str(a)+"\n")
		fout.write("C "+str(a/2.0)+" "+str((i+2)*a)+" "+str(a/2.0)+"\n")
		fout.write("C "+str(a)+" "+str(((i+2)*a)-a/2.0)+" "+str(a/2.0)+"\n")
		fout.write("C "+"0.0"+" "+str(((i+2)*a)-a/2.0)+" "+str(a/2.0)+"\n")
		fout.write("C "+str(a/2.0)+" "+str(((i+2)*a)-a/2.0)+" "+str(a)+"\n")
		fout.write("C "+str(a/2.0)+" "+str(((i+2)*a)-a/2.0)+" "+"0.0"+"\n")
		for j in xrange(0,px+1):
			fout.write("C "+str(a+j*a)+" "+str((i+2)*a)+" 0.0\n")
			fout.write("C "+str(a+j*a)+" "+str((i+2)*a)+" "+str(a)+"\n")
			fout.write("C "+str(j*a+a/2.0)+" "+str((i+2)*a)+" "+str(a/2.0)+"\n")
			fout.write("C "+str(a+j*a)+" "+str(((i+2)*a)-a/2.0)+" "+str(a/2.0)+"\n")
			fout.write("C "+str(j*a+a/2.0)+" "+str(((i+2)*a)-a/2.0)+" "+str(a)+"\n")
			fout.write("C "+str(j*a+a/2.0)+" "+str(((i+2)*a)-a/2.0)+" "+"0.0"+"\n")
		
if(pz!=0):
	for i in xrange(0,pz):
		fout.write("C "+str(a)+" 0.0 "+str((i+2)*a)+"\n")
		fout.write("C 0.0 0.0 "+str((i+2)*a)+"\n")
		fout.write("C 0.0 "+str(a)+" "+str((i+2)*a)+"\n")
		fout.write("C "+str(a)+" "+str(a)+" "+str((i+2)*a)+"\n")
		fout.write("C "+str(a/2.0)+" "+str(a/2.0)+" "+str((i+2)*a)+"\n")
		fout.write("C "+str(a/2.0)+" "+str(a)+" "+str(((i+2)*a)-a/2.0)+"\n")
		fout.write("C "+str(a/2.0)+" "+"0.0"+" "+str(((i+2)*a)-a/2.0)+"\n")
		fout.write("C "+str(a)+" "+str(a/2.0)+" "+str(((i+2)*a)-a/2.0)+"\n")
		fout.write("C "+"0.0"+" "+str(a/2.0)+" "+str(((i+2)*a)-a/2.0)+"\n")
		for j in xrange(0,py+1):
			fout.write("C 0.0 "+str(a+j*a)+" "+str((i+2)*a)+"\n")
			fout.write("C "+str(a)+" "+str(a+j*a)+" "+str((i+2)*a)+"\n")
			fout.write("C "+str(a/2.0)+" "+str(j*a+a/2.0)+" "+str((i+2)*a)+"\n")
			fout.write("C "+str(a/2.0)+" "+str(a+j*a)+" "+str(((i+2)*a)-a/2.0)+"\n")
			fout.write("C "+str(a)+" "+str(j*a+a/2.0)+" "+str(((i+2)*a)-a/2.0)+"\n")
			fout.write("C "+"0.0"+" "+str(j*a+a/2.0)+" "+str(((i+2)*a)-a/2.0)+"\n")
			for k in xrange(0,px+1):
				fout.write("C "+str(a+k*a)+" 0.0 "+str((i+2)*a)+"\n")
				fout.write("C "+str(a+k*a)+" "+str(a+j*a)+" "+str((i+2)*a)+"\n")
				fout.write("C "+str(k*a+a/2.0)+" "+str(j*a+a/2.0)+" "+str((i+2)*a)+"\n")
				fout.write("C "+str(k*a+a/2.0)+" "+str(a+j*a)+" "+str(((i+2)*a)-a/2.0)+"\n")
				fout.write("C "+str(k*a+a/2.0)+" "+"0.0"+" "+str(((i+2)*a)-a/2.0)+"\n")
				fout.write("C "+str(a+k*a)+" "+str(j*a+a/2.0)+" "+str(((i+2)*a)-a/2.0)+"\n")
		
fout.close()

print "\n----------------------------------"
print "'"+outfile+".xyz' Successfully created !!"
print "----------------------------------\n"

